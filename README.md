
# Building the Video Capturing container

## Introduction
This document guides the user to build and deploy the video capturing container.

## Pre-Requisites
- ARM Based Embedded Platform. The following are recommended:
    - Rockchip 3399ProD
    - NXP i.MX 8M Plus
- USB Pen Drive Flashed with ARM System-Ready EWAOL Image with preinstalled docker.

It is recommended to build and run the video capturing container before the inference and application containers.

## Steps overview
- Clone the repository
- Building and running the docker container with usb webcam
- Building and running the docker container with MP4 video file
- Next steps

Note that the docker container can run either with a usb webcam or a MP4 video file. Choose the most appropriate one.

## Clone the repository

- Clone the gitlab repo using the below command.
```
git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
## Building and running the docker container with usb webcam
- Change the working directory to the Video Capturing container folder.
```
cd <PROJECT_ROOT>
```
- Build the docker container.
    ```sh
    docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
    ```
    Update IMAGE_NAME and IMAGE_TAG. These can be given according to user choice e.g. "Video_capturing_container:v1".

- Use the below command to run the docker container to take input from usb webcam.
    ```sh
    docker run -it --name video_stream --network=host --device=/dev/video0 <IMAGE_NAME>:<IMAGE_TAG> ./rtsp-simple-server usb_webcam.yml
    ```
    Update the image and tag name accordingly to the image previously built.

## Building and running the docker container with MP4 video file
- Change the working directory to the Video Capturing container folder.
```
cd <PROJECT_ROOT>
```
- Copy a MP4 video file inside the project folder.
```
cp <PATH_TO_MP4_VIDEO_FILE> <ROOT_OF_PROJECT_FOLDER>/video.mp4
```
- Open the Dockerfile.
    - Add a COPY instruction to copy the video file to the container.

        Note that you might only need to uncomment the last line of the Dockerfile.
    ```
    COPY <PATH_TO_MP4_VIDEO_FILE> video.mp4
    ```
    - Save the file.
- Build the docker container.
    ```
    docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
    ```
    Update IMAGE_NAME and IMAGE_TAG. These can be given according to user choice e.g. "Video_capturing_container:v1".
- Run the docker container.
    ```
    docker run -it --name video_stream --network=host <IMAGE_NAME>:<IMAGE_TAG> ./rtsp-simple-server mp4.yml
    ```
    Update the image and tag name accordingly to the image previously built.

    This will start the RTSP streaming server at port number 8554/cam, the user can play with the stream with the below link.
    ```
    rtsp://127.0.0.1:8554/cam
    ```
## Next steps
- Clone and build the inference container.
